import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [
  {
    path: '/auth',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: 'miner-register', component: () => import('pages/miner/MinerRegister.vue') },
      { path: 'miner-check-connection', component: () => import('pages/miner/MinerCheckConnection.vue') },
      { path: 'miner-verify', component: () => import('pages/miner/MinerVerify.vue') },
      { path: '', component: () => import('pages/Auth.vue') }
    ]
  },
  {
    path: '/miner',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'select-datasets', component: () => import('pages/miner/MinerSelectDatasets.vue') },
      { path: '', component: () => import('pages/miner/MinerDashboard.vue') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'lotus-connection', component: () => import('pages/LotusTestConnection.vue') },
      { path: 'lotus-enter-token', component: () => import('pages/LotusEnterToken.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
