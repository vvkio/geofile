from flask import Flask
from flasgger import Swagger
from route.home import home_api
from redis import Redis
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database, drop_database
from flask_seeder import FlaskSeeder
import flask_praetorian

import rq
import os

# Config
app = Flask(__name__)
app.config.from_pyfile('config.py')

# Database
app.config.from_object(os.environ.get('FLASK_CONFIG', 'config.DevelopmentConfig'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

migrate = Migrate(app, db)
from models.user import User

# Init praetorian
app.config['SECRET_KEY'] = os.environ['APP_SECRET_KEY']
guard = flask_praetorian.Praetorian()
guard.init_app(app, User)

# Seeder
seeder = FlaskSeeder()
seeder.init_app(app, db)

@app.cli.command('drop-db')
def drop_db():
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    if database_exists(engine.url):
        drop_database(engine.url)

    print(not database_exists(engine.url))

@app.cli.command('create-db')
def create_db():
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    if not database_exists(engine.url):
        create_database(engine.url)

    print(database_exists(engine.url))

def create_app():
    app.config['SWAGGER'] = {
        'title': 'Flask API Starter Kit',
    }

    app.config['REDIS_URL'] = os.environ.get('REDIS_URL') or 'redis://localhost:6379'
    app.config['JWT_ACCESS_LIFESPAN'] = {'hours': 24}
    app.config['JWT_REFRESH_LIFESPAN'] = {'days': 30}

    app.redis = Redis.from_url(app.config['REDIS_URL'])
    app.task_queue = rq.Queue('microblog-tasks', connection=app.redis)

    swagger = Swagger(app)

    app.register_blueprint(home_api, url_prefix='/api')

    return app


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app = create_app()

    app.run(host='0.0.0.0', port=port)
