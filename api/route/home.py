# @app.route('/hello/', methods=['GET', 'POST'])
from http import HTTPStatus
from flask import Blueprint
from flasgger import swag_from
from models.welcome import WelcomeModel
from schema.welcome import WelcomeSchema
from pygate_grpc.client import PowerGateClient

home_api = Blueprint('api', __name__)

@home_api.route('/')
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Welcome to the Flask Starter Kit',
            'schema': WelcomeSchema
        }
    }
})

def welcome():
    """
    1 liner about the route
    A more detailed description of the endpoint
    ---
    """
    result = WelcomeModel()
    # client = PowerGateClient("127.0.0.1:5002")
    # balance = client.wallet.balance('f3...address')
    # result.powergate_status = balance
    return WelcomeSchema().dump(result), 200