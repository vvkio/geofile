# @app.route('/upload_file/', methods=['GET', 'POST'])
from http import HTTPStatus
from flask import Blueprint
from flasgger import swag_from
from models.dataset import Dataset
from pygate_grpc.client import PowerGateClient
from pygate_grpc.ffs import get_file_bytes, bytes_to_chunks


api = Blueprint('api', __name__)

@api.route('/push_file', methods=['POST'])
@swag_from({
    'responses': {
        HTTPStatus.OK.value: {
            'description': 'Welcome to the Flask Starter Kit',
            'schema': WelcomeSchema
        }
    }
})

# TODO: implement JWT or OAUTH

def push_file():
    """
    Endpoint for internal GeoFile batch processing
    This endpoint will be called for each dataset datafile
    ---
    """
    # result = WelcomeModel()
    # client = PowerGateClient("127.0.0.1:5002")
    # balance = client.wallet.balance('f3...address')
    # result.powergate_status = balance
    data = request.json
    powergate = PowerGateClient("geofile-powergate.filmine.io:5002")

    # Create instance
    ffs = powergate.ffs.create()

    # TODO: figoure out how to ensure secure dataset storage before it reaches the IPFS or FileCoin storage
    dataset_filepath = dataset.id + data.filepath
    # OR pull via CID... not decided yet

    iter = get_file_bytes(dataset_filepath)
    print("pushing to IPFS")
    res = powergate.ffs.stage(bytes_to_chunks(iter), ffs.token)

    powergate.ffs.push(res.cid, override=False, token=ffs.token)
    addresses = powergate.ffs.addrs_list(ffs.token)
    wallet = addresses.addrs[0].addr

    # TODO: pull TrusterMiner[] from db ["t01000","t02000"]
    countryCodes = dataset.country_codes()
    trustedMiners = # TODO: find all miners from countries that belong to this dataset
    # and the miners dataset preference matches this dataset
    new_config = (
        '{"hot":{"enabled":true,"allowUnfreeze":true,"ipfs":{"addTimeout":30}},'
        '"cold":{"enabled":true,"filecoin":{"repFactor":1,"dealMinDuration":518400,'
        '"excludedMiners":["t01101"],"trustedMiners":["t01000","t02000"],'
        '"countryCodes":["ca","nl"],"renew":{"enabled":true,"threshold":3},'
        '"addr":"' + wallet + '","maxPrice":50}},"repairable":true}'
    )
    powergate.ffs.push(res.cid, override=True, config=new_config, token=ffs.token)

    check_cid_pinned = powergate.ffs.info(res.cid, ffs.token)
    print("Checking FFS pins")
    print(check)

    # TODO: save Dataset, DataSet category and CID to database
    #
    # return WelcomeSchema().dump(result), 200