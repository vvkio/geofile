# GeoFile Backend

Backend to handle and secure communication between the user and powergate instances. At the moment the codebase contains minimal examples for the purposes of the ETHGlobal hackaton 2020.

## Development environment

```bash
pip install -r requirements.txt
docker-compose up
bin/flask drop-db # (in case you have to wipe the db)
bin/flask create-db
bin/flask db init
bin/flask db migrate
bin/flask db upgrade
bin/flask seed run
bin/dev-server
```

**psycopg2 troubleshooting on OSX where `openssl` is installed via HomeBrew**

```bash
LDFLAGS="-L$(brew --prefix openssl)/lib" CFLAGS="-I$(brew --prefix openssl)/include" pip install psycopg2
```

