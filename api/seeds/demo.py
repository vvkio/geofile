from flask_seeder import Seeder, Faker, generator
import flask_praetorian
from app import guard
from models.user import User

class DemoSeeder(Seeder):
    def run(self):
        self.db.session.add(User(
            username = "vvk",
            password=guard.hash_password('abides'),
            roles="admin",
            is_active=True
        ))
        self.db.session.commit()