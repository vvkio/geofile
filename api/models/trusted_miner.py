from sqlalchemy.dialects.postgresql import JSON
from app import db

class TrustedMiner(db.Model):
    __tablename__ = 'trusted_miners'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.Text, unique=True)
    password = db.Column(db.Text)
    roles = db.Column(db.Text)
    miner_id = db.Column(db.Text)
    miner_address = db.Column(db.Text)
    country_code = db.Column(db.CHAR, length=4)
    city = db.Column(db.Text)
    is_verified = db.Column(db.Boolean, default=True, server_default='true')

    @property
    def rolenames(self):
        try:
            return self.roles.split(',')
        except Exception:
            return []

    @classmethod
    def lookup(cls, email):
        return cls.query.filter_by(email=email).one_or_none()

